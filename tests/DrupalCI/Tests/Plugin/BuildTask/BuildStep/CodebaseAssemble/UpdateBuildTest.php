<?php

namespace DrupalCI\Tests\Plugin\BuildTask\BuildStep\CodebaseAssemble;

use DrupalCI\Build\Codebase\CodebaseInterface;
use DrupalCI\Tests\DrupalCITestCase;
use org\bovigo\vfs\vfsStream;

/**
 * @coversDefaultClass \DrupalCI\Plugin\BuildTask\BuildStep\CodebaseAssemble\UpdateBuild
 */
class UpdateBuildTest extends DrupalCITestCase {

  /**
   * Test that plugin is discoverable and can instantiate, and returns 0 for
   * no changed files.
   *
   * @covers ::run
   */
  public function testRun() {
    $plugin_factory = $this->getContainer()['plugin.manager.factory']->create('BuildTask');
    $plugin = $plugin_factory->getPlugin('BuildStep', 'update_build', []);
    $this->assertSame(0, $plugin->run());
  }

  public function provideShouldReplace() {
    return [
      // [expected outcome, project type, config dir, path to drupalci.yml, is drupalci.yml modified?]
      'no-drupalciyml' => [FALSE, 'core','core', '', FALSE],
      'modified-drupalciyml' => [TRUE, 'core', 'core', 'core/drupalci.yml', TRUE],
      'modified-contrib-drupalciyml' => [TRUE, 'module', '', 'drupalci.yml', TRUE],
      'unmodified-drupalciyml' => [FALSE, 'core', 'core', 'core/drupalci.yml', FALSE],
      'unmodified-contrib-drupalciyml' => [FALSE, 'module', '', 'drupalci.yml', FALSE],
    ];
  }

  /**
   * @covers ::shouldReplaceAssessmentStage
   * @dataProvider provideShouldReplace
   */
  public function testShouldReplaceAssessmentStage($expected, $project_type, $config_dir, $drupalci_yml_path, $modified) {
    $codebase = $this->getMockBuilder(CodebaseInterface::class)
      ->setMethods(['getModifiedFiles', 'getProjectType', 'getProjectConfigDirectory'])
      ->getMockForAbstractClass();

    $modified_files = [];
    if ($modified) {
      $modified_files = [$drupalci_yml_path];
    }


    $vfs = vfsStream::setup('source', NULL, [$drupalci_yml_path => '']);
    if (empty($drupalci_yml_path)){
      $drupalci_yml_path = 'nofile.yml';
    }
    $drupalci_yml_mock = $vfs->url() . '/' . $drupalci_yml_path;


    $codebase->expects($this->any())
      ->method('getProjectType')
      ->willReturn($project_type);
    $codebase->expects($this->once())
      ->method('getModifiedFiles')
      ->willReturn($modified_files);
    $codebase->expects($this->any())
      ->method('getProjectConfigDirectory')
      ->willReturn($config_dir);

    $container = $this->getContainer(['codebase' => $codebase]);
    $plugin_factory = $container['plugin.manager.factory']->create('BuildTask');
    $plugin = $plugin_factory->getPlugin('BuildStep', 'update_build');

    $ref_should = new \ReflectionMethod($plugin, 'shouldReplaceAssessmentStage');
    $ref_should->setAccessible(TRUE);

    $this->assertEquals($expected, $ref_should->invokeArgs($plugin, [$drupalci_yml_mock]));
  }

}
