<?php

namespace DrupalCI\Tests\Application\Phpcs;

use DrupalCI\Tests\DrupalCIFunctionalTestBase;
use Symfony\Component\Console\Tester\ApplicationTester;

/**
 * Test that we sniff the entire project if the phpcs config file is modified.
 *
 * NOTE: This test assumes you have followed the setup instructions in TESTING.md
 *
 * @group Application
 * @group phpcs
 *
 * @see TESTING.md
 */
class CoreD8CoderPhpcsConfigModifiedTest extends DrupalCIFunctionalTestBase {


  public function testPhpcsConfigModified() {

    $options = ['interactive' => FALSE];
    $this->app_tester->run([
      'command' => 'run',
      'definition' => 'tests/DrupalCI/Tests/Application/Fixtures/build.CoreD8CoderPhpcsConfigModified.yml',
    ], $options);
    $this->assertRegExp('/The installed coding standards are .* Drupal/', $this->app_tester->getDisplay());
    $this->assertNotRegExp('/Running PHP Code Sniffer review on modified files./', $this->app_tester->getDisplay());
    $this->assertRegExp('/PHPCS config file modified, sniffing entire project./', $this->app_tester->getDisplay());
    // Commit hash 4b65a2b always fails CS review.
    $this->assertEquals(1, $this->app_tester->getStatusCode());
  }

}
