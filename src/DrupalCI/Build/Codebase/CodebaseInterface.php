<?php

namespace DrupalCI\Build\Codebase;

interface CodebaseInterface {

  public function addPatch(PatchInterface $patch);

  /**
   * Get a list of files modified by the patch.
   *
   * @return string[]
   */
  public function getModifiedFiles();

  /**
   * Returns an array of modified php files, relative to the source directory.
   */
  public function getModifiedPhpFiles();

  public function addModifiedFile($filename);

  public function addModifiedFiles($files);

  /**
   * This is the codebase that we will test. It should be volume mounted over
   * to wherever the $execContainerSourceDir is set on the Environment object.
   * It proxies through to the build, since the build is the directory master.
   *
   * @return string
   */
  public function getSourceDirectory();

  /**
   * The name of the project under test.
   *
   * @return string
   */
  public function getProjectName();

  public function setProjectName($projectName);

  /**
   * The type of the project under test - core, module, theme, distribution,
   * library etc.
   *
   * @return string
   */
  public function getProjectType();

  public function setProjectType($projectName);

  /**
   * The branch or tag of the project we're testing
   *
   * @return string
   */
  public function getProjectReference();

  public function setProjectReference($reference);

  /**
   * The url of the VCS repo we're cloning
   *
   * @return string
   */
  public function getProjectRepository();

  public function setProjectRepository($repository);

  /**
   * The type of reference we're testing. branch/tag/feature-branch/merge-request
   *
   * @return string
   */
  public function getProjectRefType();

  public function setProjectRefType($reftype);

  /**
   * The target branch that a Merge Request is directed towards.
   *
   * @return string
   */
  public function getProjectTargetBranch();

  public function setProjectTargetBranch(string $branch);

  /**
   * The namespace of the project at the composer facade
   *
   * @return string
   */
  public function getProjectComposerNamespace();

  public function setProjectComposerNamespace($namespace);

  /**
   * The url of the VCS repo we're cloning
   *
   * @return string
   */
  public function getProjectCommitHash();

  public function setProjectCommitHash($hash);

  /**
   * For contributed modules, this is where the modules will get checked out
   * Needed so we can know where to run the tests.
   * It is a key value array of extension type to path location
   *
   * @return array
   */
  public function getExtensionPaths();

  public function setExtensionPaths($extensionPaths);

  public function getComposerDevRequirements();

  public function getInstalledComposerPackages();

  /**
   * Path on the host directory that is the 'root' of the project
   * under test. Defaults to absolute. Pass in FALSE for relative paths.
   *
   * For core this is equivalent to DRUPAL_ROOT. For extensions this will be
   * where composer-installers would place the project.
   *
   * @param bool $absolute
   *
   * @return string
   */
  public function getProjectSourceDirectory($absolute);

  /**
   * Absolute path on the host where the config files is located, per project
   *
   * Basically translates into 'core' or the extension source dir.
   *
   * @param bool $absolute
   *
   * @return string
   */
  public function getProjectConfigDirectory($absolute);

}
