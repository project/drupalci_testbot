<?php

namespace DrupalCI\Plugin\BuildTask\BuildStep\CodebaseAssemble;

use Composer\Json\JsonFile;
use DrupalCI\Plugin\BuildTask\BuildStep\BuildStepInterface;
use DrupalCI\Plugin\BuildTask\BuildTaskInterface;

/**
 * @PluginID("gather_dependencies_d7")
 */
class GatherDependenciesD7 extends GatherDependencies implements BuildStepInterface, BuildTaskInterface {

  /* @var \DrupalCI\Build\Codebase\CodebaseInterface */
  protected $codebase;

  protected $drupalPackageRepository = 'https://packages.drupal.org/7';

  /**
   * @inheritDoc
   */
  public function run() {
    if ($this->codebase->getProjectType() === 'core' ) {
      // We dont need to do anything for core testing in this plugin.
      return 0;
    }
    $this->setupD7Composer();

    parent::run();
  }

  protected function setupD7Composer(): void {
    $source_dir = $this->environment->getExecContainerSourceDir();
    $cmd = "sudo -u www-data /usr/local/bin/composer init --name \"drupal/drupal\" --type \"drupal-core\" -n --working-dir " . $source_dir;
    $this->io->writeln("Initializing composer repository");
    $this->execRequiredEnvironmentCommands($cmd, 'Composer init failure');

    $cmd = "sudo -u www-data /usr/local/bin/composer config discard-changes true --working-dir " . $source_dir;;
    $this->io->writeln("Ignoring Composer Changes");
    $this->execRequiredEnvironmentCommands($cmd, 'Composer config failure');

    $cmd = "sudo -u www-data /usr/local/bin/composer config minimum-stability dev --working-dir " . $source_dir;
    $this->io->writeln("Setting Minimum Stability");
    $this->execRequiredEnvironmentCommands($cmd, 'Composer config failure');

    $cmd = "sudo -u www-data /usr/local/bin/composer config prefer-stable true --working-dir " . $source_dir;
    $this->io->writeln("Setting Preferred Stability");
    $this->execRequiredEnvironmentCommands($cmd, 'Composer config failure');

    $cmd = "sudo -u www-data /usr/local/bin/composer config version 7.59 --working-dir " . $source_dir;
    $this->io->writeln("Setting Arbitrarily High d7 version");
    $this->execRequiredEnvironmentCommands($cmd, 'Composer config failure');

    $cmd = "sudo -u www-data /usr/local/bin/composer config allow-plugins.composer/installers true --working-dir " . $source_dir;
    $this->io->writeln("Add composer/installers to allow-plugins config (will fail on Composer 1)");
    $this->execEnvironmentCommands($cmd);

    $cmd = "sudo -u www-data /usr/local/bin/composer config allow-plugins.dealerdirect/phpcodesniffer-composer-installer true --working-dir " . $source_dir;
    $this->io->writeln("Add dealerdirect/phpcodesniffer-composer-installer to allow-plugins config (will fail on Composer 1)");
    $this->execEnvironmentCommands($cmd);

    $cmd = "sudo -u www-data /usr/local/bin/composer require composer/installers --working-dir " . $source_dir;
    $this->io->writeln("Composer Command: $cmd");
    $this->execRequiredEnvironmentCommands($cmd, 'Composer require failure');

    $cmd = "sudo -u www-data /usr/local/bin/composer config repositories.pdo composer {$this->drupalPackageRepository} --working-dir ${source_dir}";
    $this->io->writeln("Adding packages.drupal.org as composer repository");
    $this->execRequiredEnvironmentCommands($cmd, 'Composer config failure');

    $composer_json = $this->codebase->getSourceDirectory() . '/composer.json';
    if (file_exists($composer_json)) {
      $composerFile = new JsonFile($composer_json);
      $composer_config = $composerFile->read();
      foreach ($this->codebase->getExtensionPaths() as $extension_type => $path) {
        $path = $path . '/{$name}';
        $extension_type = rtrim($extension_type, 's');
        $composer_config['extra']['installer-paths'][$path] = ["type:drupal-$extension_type"];
      }
      $cmd = "sudo -u www-data chmod 0777 {$composer_json}";
      $result = $this->execRequiredCommands($cmd, 'Cannot remove composer.json');
      $composerFile->write($composer_config);
    }
  }


}
