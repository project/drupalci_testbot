<?php

namespace DrupalCI\Plugin\BuildTask\BuildStep\CodebaseAssemble;

use DrupalCI\Injectable;
use DrupalCI\Plugin\BuildTask\BuildStep\BuildStepInterface;
use DrupalCI\Plugin\BuildTaskBase;
use DrupalCI\Plugin\BuildTask\BuildTaskInterface;
use Pimple\Container;
use Composer\Json\JsonFile;


/**
 * @PluginID("gather_dependencies")
 */
class GatherDependencies extends BuildTaskBase implements BuildStepInterface, BuildTaskInterface, Injectable {

  protected $drupalPackageRepository = 'https://packages.drupal.org/8';

  protected $executable_path = 'sudo -u www-data /usr/local/bin/composer';

  public function inject(Container $container) {
    parent::inject($container);
    $this->codebase = $container['codebase'];

  }

  /**
   * Codebase.
   *
   * @var \DrupalCI\Build\Codebase\CodebaseInterface
   */
  protected $codebase;

  /**
   * @inheritDoc
   */
  public function run() {
    // Assuming we have a git clone of a project to test, and the patch has
    // Been applied (if necessary)
    // If the project has changes to composer.json then:
    //  Commit any changes introduced by patches.
    //  Add the project as a vcs repo to root composer.json
    //
    // Then always:
    //  Composer require the project
    //  Composer require any of its dev-dependencies
    //
    // Finally, If the project does not have changes to composer.json then
    //  Copy the ancillary contrib dir over to the existing dir in the drupal
    //  codebase.
    if ('TRUE' === strtoupper(getenv('DCI_Debug'))) {
      $verbose = ' -vvv';
      $progress = '';
    } else {
      $verbose = '';
      $progress = ' --no-progress';
    }
    $modified_files = $this->codebase->getModifiedFiles();
    $has_modified_composer_json = in_array("composer.json", $this->codebase->getModifiedFiles());
    $source_dir = $this->environment->getExecContainerSourceDir();
    $composer_projectname = $this->codebase->getProjectComposerNamespace();
    $is_feature_mr = in_array($this->codebase->getProjectRefType(), ['feature-branch','merge-request']);
    $has_composer_json = file_exists("{$this->build->getProjectVCSRepoDirectory()}/composer.json");
    $project_commit_hash = $this->codebase->getProjectCommitHash();

    if ($this->codebase->getProjectType() === 'core' ) {
      return 0;
    }

    if ($this->configuration['add-facade']) {
      // Makes sure that p.d.o. is there for old drupal tests.
      $cmd = "{$this->executable_path} ${verbose}config repositories.pdo composer {$this->drupalPackageRepository} --working-dir ${source_dir}";
      $this->io->writeln("Adding packages.drupal.org as composer repository");
      $this->execRequiredEnvironmentCommands($cmd, 'Composer config failure');
    }
    // If the maintainer is modifying composer.json, we are assuming that they
    // have a complete composer.json, and that it can be relied on for
    // dependency resolution. We will *also* assume this to be true for
    // Merge request testing and feature branch tests.

    // TODO: To support feature branches, we have to set the modified files properly
    if (!$is_feature_mr && in_array("composer.json", $modified_files)) {

      // Commit the changes if the patch changed anything.
      $cmd = "cd " . $this->environment->getContainerVCSRepoDir() . " && sudo -u www-data git add . && sudo -u www-data git config --global user.email \"drupalci@drupalci.org\" &&
sudo -u www-data git config --global user.name \"The Testbot\" && sudo -u www-data git commit -am 'intermediate commit'";
      $this->io->writeln("Git Command: $cmd");
      $result = $this->execRequiredEnvironmentCommands($cmd, 'Project dir commit failure');

    }
    // @TODO: Bots need to run at least composer 2.2.15 for below command to work, or else
    // we will encounter this bug: https://github.com/composer/composer/issues/10907
    // bots are currently on 2.2.14 so commenting out until we can fix
    // $cmd = "sudo -u www-data /usr/local/bin/composer${verbose} config allow-plugins true -n --working-dir " . $source_dir;
    // $this->io->writeln("Allow all plugins, since this is running in a CI environment");
    // $this->execEnvironmentCommands($cmd);

    if (!$is_feature_mr || $has_composer_json) {
      // Composer require
      // If we have a modified composer.json, or this is a feature branch
      // or merge request test and there is a composer.json in the repo,
      if (($is_feature_mr && $has_composer_json) || $has_modified_composer_json) {
        $this->setComposerRepo();
        $version = "{$this->codebase->getProjectReference()}-dev";
      } else {
        $version = $this->getSemverBranch($this->codebase->getProjectReference());
      }
      if (!empty($project_commit_hash)) {
        $version = "{$version}#{$project_commit_hash}";
      }
      $cmd = "{$this->executable_path}${verbose} config platform --unset --working-dir ${source_dir}";
      $this->io->writeln("Composer Command: $cmd");
      $result = $this->execRequiredEnvironmentCommands($cmd, 'Composer config failure');

      $cmd = "{$this->executable_path}${verbose} require drupal/${composer_projectname} {$version} {$this->configuration['options']} --working-dir ${source_dir}";

      $this->io->writeln("Composer Command: $cmd");
      $result = $this->execRequiredEnvironmentCommands($cmd, 'Composer require failure');
    }


    // Look for changes to require dev as well. These require-dev packages will
    // Only exist if there exists dev-depenencies in composer.json
    // Otherwise we will not see anything in the root dir
    $packages = $this->codebase->getComposerDevRequirements();

    if (!empty($packages)) {
      $cmd = "{$this->executable_path}${verbose} require " . implode(" ", $packages) . " {$this->configuration['options']} --working-dir " . $source_dir;
      $this->io->writeln("Composer Command: $cmd");
      $this->execRequiredEnvironmentCommands($cmd, 'Composer require-dev failure');

    }

    // If the local clone didnt modify composer.json, we didnt use it for
    // dependency resolution, which means our workdir has the contrib project
    // checked out, but unpatched, so we copy it back into place.
    if ((!$has_modified_composer_json && !$is_feature_mr) || ($is_feature_mr && !$has_composer_json)) {
      $contrib_dir = $this->codebase->getProjectSourceDirectory();
      $vcs_dir = $this->build->getProjectVCSRepoDirectory();
      if (file_exists($contrib_dir)){
        $cmds[] =  "sudo -u www-data rm -rf $contrib_dir";
      }
      $parent_folder = dirname($contrib_dir);
      $cmds[] =  "sudo -u www-data mkdir -p {$parent_folder}";
      $cmds[] =  "sudo -u www-data mv $vcs_dir $contrib_dir";
      $result = $this->execRequiredCommands($cmds, 'Contrib Copy Failure');
    }

  }

  /**
   * Converts a drupal branch string that is stored in git into a composer
   * based branch string. For d8 contrib
   *
   * @param $branch
   *
   * @return mixed
   */
  protected function getSemverBranch($branch) {
    return preg_replace('/^\d+\.x-/', '', $branch) . '-dev';
  }

  /**
   *
   * @throws \DrupalCI\Plugin\BuildTask\BuildTaskException
   */
  private function setComposerRepo() {
    // Point our installation at this vcsRepoDir so we can composer require it.
    $root_composer_json = "{$this->build->getSourceDirectory()}/composer.json";
    $composerFile = new JsonFile($root_composer_json);
    $composer_config = $composerFile->read();
    $repositories = $composer_config['repositories'];
    $local_repo = [
      'type' => 'vcs',
      'url' => $this->environment->getContainerVCSRepoDir()
    ];

    array_unshift($repositories, $local_repo);
    $composer_config['repositories'] = $repositories;
    $cmd = "sudo -u www-data chmod 0777 {$root_composer_json}";
    $result = $this->execRequiredCommands($cmd, 'Cannot chmod composer.json');
    $composerFile->write($composer_config);
  }

  /**
   * @inheritDoc
   */
  public function getDefaultConfiguration() {
    $progress = 'TRUE' === strtoupper(getenv('DCI_Debug')) ? '' : ' --no-progress';
    return [
      'options' => "--prefer-stable${progress} --prefer-dist --no-suggest --no-interaction",
      'add-facade' => FALSE,
    ];
  }
}
