<?php

namespace DrupalCI\Plugin\BuildTask\BuildStep\CodebaseAssemble;

use DrupalCI\Injectable;
use DrupalCI\Plugin\BuildTask\BuildStep\BuildStepInterface;
use DrupalCI\Plugin\BuildTask\FileHandlerTrait;
use DrupalCI\Plugin\BuildTaskBase;
use DrupalCI\Plugin\BuildTask\BuildTaskInterface;
use Pimple\Container;

/**
 * @PluginID("checkout")
 */
class Checkout extends BuildTaskBase implements BuildStepInterface, BuildTaskInterface, Injectable {

  use FileHandlerTrait;
  /* @var \DrupalCI\Build\Codebase\CodebaseInterface */
  protected $codebase;

  public function inject(Container $container) {
    parent::inject($container);
    $this->codebase = $container['codebase'];

  }

  /**
   * @inheritDoc
   */
  public function configure() {
    if (FALSE !== getenv('DCI_Checkout_Repo')) {
      $repo['repo'] = getenv('DCI_Checkout_Repo');

      if (FALSE !== getenv('DCI_Checkout_Branch')) {
        $repo['branch'] = getenv('DCI_Checkout_Branch');
      }
      if (FALSE !== getenv('DCI_Checkout_Hash')) {
        $repo['commit-hash'] = getenv('DCI_Checkout_Hash');
      }
      $this->configuration['repositories'][0] = $repo;
    }

  }

  /**
   * @inheritDoc
   */
  public function run() {
    $this->io->writeln("<info>Checking out git repos.</info>");
    foreach ($this->configuration['repositories'] as $repository ) {
      $this->io->writeln("<info>Entering setup_checkout_git().</info>");
      // @TODO: these should always have a default. no sense in setting them here.
      $repo = $repository['repo'];

      $git_branch = isset($repository['branch']) ? "-b " . $repository['branch'] : '';
      $git_depth = '';

      /*
       * On modern versions of composer using modern git, composer will
       * cache a vcsrepo and then assume its a full repo.
       * So we have to have a full repo until we can figure out some other way
      if (empty($repository['commit-hash'])) {
        $git_depth = '--depth=2';
      }
      */
      if (isset($repository['checkout-dir'])) {
        $directory = $repository['checkout-dir'];
      }
      elseif ($this->codebase->getProjectType() == 'core') {
        $directory = $this->codebase->getSourceDirectory();
      }
      else {
        $directory = $this->build->getProjectVCSRepoDirectory();
      }
      if (isset($repository['merge-request'])) {


        $cmd = "sudo -u www-data git init '$directory'";
        $this->io->writeln("Git Command: $cmd");
        $this->execRequiredCommands($cmd, 'Initialization Error');

        $cmd = "sudo -u www-data git -C '$directory' remote add origin $repo";
        $this->io->writeln("Git Command: $cmd");
        $this->execRequiredCommands($cmd, 'git remote add Error');


        $cmd = "sudo -u www-data git -C '$directory' fetch origin $git_depth merge-requests/{$repository['merge-request']}/merge:{$this->codebase->getProjectReference()}";
        $this->io->writeln("Git Command: $cmd");
        $this->execRequiredCommands($cmd, 'Fetch Error');

        $cmd = "sudo -u www-data git -C '$directory' checkout {$this->codebase->getProjectReference()}";
        $this->io->writeln("Git Command: $cmd");
        $this->execRequiredCommands($cmd, 'Clone Error');

        $cmd = "sudo -u www-data git -C '$directory' diff --name-only HEAD HEAD~1";
        $this->io->writeln("Git Command: $cmd");
        $result = $this->execRequiredCommands($cmd, 'Clone Error');
        $files = explode("\n", $result->getOutput());
        $this->codebase->addModifiedFiles($files);

        $cmd = "sudo -u www-data git -C '$directory' rev-list --parents -n 1 HEAD";
        $this->io->writeln("Git Command: $cmd");
        $result = $this->execRequiredCommands($cmd, 'Error getting rev-list');
        $cmdoutput = $result->getOutput();
        $this->io->writeln("<comment>Git rev-list:</comment>");
        $this->io->writeln("<comment>\t${cmdoutput}");
        $this->io->writeln("<comment>End of Git rev-list</comment>");
      }
      else {

        $this->io->writeln("<comment>Performing git checkout of $repo $git_branch to $directory.</comment>");

        $cmd = "sudo -u www-data git clone -v $git_branch $git_depth $repo '$directory'";
        $this->io->writeln("Git Command: $cmd");
        $this->execRequiredCommands($cmd, 'Checkout Error');

        if (!empty($repository['commit-hash'])) {
          $cmd = "cd " . $directory . " && sudo -u www-data git reset -q --hard " . $repository['commit-hash'] . " ";
          $this->io->writeln("Git Command: $cmd");
          $this->execRequiredCommands($cmd, 'git reset returned an error.');
        }

        $cmd = "cd '$directory' && sudo -u www-data git log --oneline -n 1 --decorate";
        $result = $this->execCommands($cmd);
        $cmdoutput = $result->getOutput();
        $this->io->writeln("<comment>Git commit info:</comment>");
        $this->io->writeln("<comment>\t${cmdoutput}");

        $this->io->writeln("<comment>Checkout complete.</comment>");
      }
    }
    return 0;
  }

  /**
   * @inheritDoc
   */
  public function getDefaultConfiguration() {
    return [
      'repositories' => [],
    ];

  }

}
