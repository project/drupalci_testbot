<?php

namespace DrupalCI\Plugin\BuildTask\BuildStage;

use DrupalCI\Plugin\BuildTask\BuildTaskInterface;
use DrupalCI\Plugin\BuildTaskBase;
use Pimple\Container;

/**
 * @PluginID("codebase")
 */

class CodebaseBuildStage extends BuildTaskBase implements BuildStageInterface, BuildTaskInterface {

  /* @var \DrupalCI\Build\Codebase\CodebaseInterface */
  protected $codebase;

  public function inject(Container $container) {
    parent::inject($container);
    $this->codebase = $container['codebase'];
  }

  /**
   * @inheritDoc
   */
  public function configure() {
    if (FALSE !== getenv(('DCI_ProjectType'))) {
      $this->configuration['project-type'] = getenv('DCI_ProjectType');
    }
    if (FALSE !== getenv(('DCI_ProjectName'))) {
      $this->configuration['project-name'] = getenv('DCI_ProjectName');
    }

  }

  /**
   * @inheritDoc
   */
  public function run() {

    if (!empty($this->configuration['project-name']) && $this->configuration['project-name'] !== 'drupal') {
      $this->codebase->setProjectName($this->configuration['project-name']);
    }
    if (!empty($this->configuration['project-type'])) {
      $this->codebase->setProjectType($this->configuration['project-type']);
    }
    if (!empty($this->configuration['repository'])) {
      $this->codebase->setProjectRepository($this->configuration['repository']);
    }
    if (!empty($this->configuration['reference'])) {
      $this->codebase->setProjectReference($this->configuration['reference']);
    }
    if (!empty($this->configuration['reference-type'])) {
      $this->codebase->setProjectRefType($this->configuration['reference-type']);
    }
    if (!empty($this->configuration['target-branch'])) {
      $this->codebase->setProjectTargetBranch($this->configuration['target-branch']);
    }
    if (!empty($this->configuration['commit-hash'])) {
      $this->codebase->setProjectCommitHash($this->configuration['commit-hash']);
    }
    if (!empty($this->configuration['composer-namespace'])) {
      $this->codebase->setProjectComposerNamespace($this->configuration['composer-namespace']);
    }

  }

  /**
   * @inheritDoc
   */
  public function complete($childStatus) {
    // The build definition can be changed during the codebase stage, because
    // drupalci.yml can be patched. Therefore we have to store the new build as
    // a final stage here.
    $this->build->saveModifiedBuildDefiniton();

    $this->saveHostArtifact($this->codebase->getSourceDirectory() . '/vendor/composer/installed.json', 'composer-installed.json');

    $project_build_dir = ['projectDirectory' => $this->codebase->getProjectSourceDirectory()];
    $this->saveStringArtifact(json_encode($project_build_dir), 'project_directory.json');
  }

  /**
   * @inheritDoc
   */
  public function getDefaultConfiguration() {
    return [
      'project-name' => '',
      'project-type' => '',
      'composer-namespace' => '',
      'reference-type' => '',
      'repository' => '',
      'reference' => '',
      'target-branch' => '',
      'commit-hash' => '',
    ];

  }

}
